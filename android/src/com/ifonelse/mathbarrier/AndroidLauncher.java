package com.ifonelse.mathbarrier;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;

public class AndroidLauncher extends FragmentActivity implements  AndroidFragmentApplication.Callbacks {

    private WebView articleContent;
    Calculo calculo;
    private View decorView;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus){
			decorView.setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE
							| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
							| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_FULLSCREEN
							| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
			);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_layout);

		decorView = getWindow().getDecorView();

		// Create libgdx fragment
		final GameFragment libgdxFragment = new GameFragment();
		calculo = new Calculo("",true);

		libgdxFragment.calculo = this.calculo;
		libgdxFragment.modo = this.getIntent().getExtras().getInt("MODO");

		articleContent = findViewById(R.id.formula_page);
		setEquacao(calculo);

		// Put it inside the framelayout (which is defined in the layout.xml file).
		getSupportFragmentManager().beginTransaction().
				add(R.id.frame_layout, libgdxFragment).
				commit();

		calculo.objMudouListener(new GoListener() {
			@Override
			public void objetoMudou(GoEvent evt) {
				setEquacao( (Calculo)evt.getSource());
			}
		});
	}

	@Override
	public void exit() {

	}

    @SuppressLint("SetJavaScriptEnabled")
    public void setEquacao(final Calculo calculo){
        articleContent.post(new Runnable() {
            @Override
            public void run() {

                articleContent.getSettings().setJavaScriptEnabled(true);

                articleContent.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        if(calculo.getCalculo().equals("")){
							view.loadUrl("javascript:replace('"+calculo.getCalculo()+"')");
						}else{

							view.loadUrl("javascript:replace('$$"+calculo.getCalculo()+"$$')");
						}
                    }
                });

                articleContent.loadUrl("file:///android_asset/formula.html");

                articleContent.setBackgroundColor(Color.TRANSPARENT);
                articleContent.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
            }
        });
    }


}
