package com.ifonelse.mathbarrier;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;

/**
 * Created by mtayl on 27/11/2017.
 */

public class GameFragment extends AndroidFragmentApplication {

    public Calculo calculo;
    public int modo = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {


        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;

        if(modo==1){
            MathBarrier mb = new MathBarrier();
            mb.calculo = this.calculo;
            return initializeForView(mb,config);
        }else if(modo == 2){
            MathBarrier_Rapido mb = new MathBarrier_Rapido();
            mb.calculo = this.calculo;
            return initializeForView(mb,config);
        }else if(modo == 3){
            MathBarrier_Niveis mb = new MathBarrier_Niveis();
            mb.calculo = this.calculo;
            return initializeForView(mb,config);
        }else if(modo == 4){
            MathBarrier_Hardcore mb = new MathBarrier_Hardcore();
            mb.calculo = this.calculo;
            return initializeForView(mb,config);
        }else{
            return null;
        }
    }
}