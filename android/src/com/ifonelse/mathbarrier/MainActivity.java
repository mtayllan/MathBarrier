package com.ifonelse.mathbarrier;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationSet;
import android.widget.ImageView;

import com.ifonelse.mathbarrier.fragments.ConfigFragment;
import com.ifonelse.mathbarrier.fragments.MainFragment;
import com.ifonelse.mathbarrier.fragments.RecordsFragment;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity{

    private View decorView;

    private ImageView btnClassico;
    private ImageView btnRank;
    private ImageView btnNiveis;
    private ImageView btnHardcore;
    private ImageView btnRapido;
    private ImageView btnConfig;

    AnimationSet growShrink;

    MediaPlayer toqueBotao;
    MediaPlayer toqueMusica;

    Preferencias preferences;

    private boolean tocaAudio=true;
    private boolean tocaMusica=true;

    FragmentManager fragmentManager;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        decorView = this.getWindow().getDecorView();

        preferences = new Preferencias(this);
        String config = preferences.buscaConfig();
        if (config == null) preferences.salvaConfig("+_+");


        String configs[] = preferences.buscaConfig().split("_");
        if(configs[0].equals("-")){
            tocaAudio = false;
        }
        if(configs[1].equals("-")){
            tocaMusica = false;
        }

        toqueBotao = MediaPlayer.create(MainActivity.this, R.raw.toque);
        toqueMusica = MediaPlayer.create(MainActivity.this, R.raw.back);
        toqueMusica.setLooping(true);
        if(tocaMusica){
            toqueMusica.start();
        }

        callMain();
    }


    public void callMain(){
        MainFragment main = new MainFragment();
        main.mainActivity = this;
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.layout, main, "Main");
        transaction.commitAllowingStateLoss();
    }


    public void callClassico(){
        new Runnable() {
            @Override
            public void run() {
                overridePendingTransition(R.anim.scale_up, R.anim.scale_up);
                Intent i = new Intent(MainActivity.this, AndroidLauncher.class);
                i.putExtra("MODO", 1);
                startActivity(i);
            }
        }.run();
    }

    public void callRapido(){
        new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, AndroidLauncher.class);
                i.putExtra("MODO", 2);
                startActivity(i);
            }
        }.run();
    }

    public void callNiveis(){
        new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, AndroidLauncher.class);
                i.putExtra("MODO", 3);
                startActivity(i);
            }
        }.run();
    }

    public void callHardcore(){
        new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, AndroidLauncher.class);
                i.putExtra("MODO", 4);
                startActivity(i);
            }
        }.run();
    }

    public void callRank(){
        RecordsFragment recordsFragment = new RecordsFragment();
        recordsFragment.mainActivity = this;
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack("Records");
        transaction.replace(R.id.layout, recordsFragment, "Records");
        transaction.commitAllowingStateLoss();
    }

    public void callConfig(){
        ConfigFragment configFragment = new ConfigFragment();
        configFragment.mainActivity = this;
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack("Config");
        transaction.replace(R.id.layout, configFragment, "Config");
        transaction.commitAllowingStateLoss();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(tocaMusica)toqueMusica.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(tocaMusica)toqueMusica.start();
    }
}
