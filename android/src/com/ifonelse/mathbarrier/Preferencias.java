package com.ifonelse.mathbarrier;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mtayl on 27/11/2017.
 */

public class Preferencias {

    private Context context;
    private SharedPreferences sharedPreferences;
    private final String NOME_ARQUIVO = "MATHBARRIER-PREFERENCIAS";
    private final int MODE = 0;
    private SharedPreferences.Editor editor;

    private final String RECORDS = "records";
    private final String RECORDS2 = "records_rapido";
    private final String RECORDS3 = "records_niveis";
    private final String RECORDS4 = "records_hardcore";

    private final String CONFIG = "configuracoes";

    public Preferencias(Context context){
        this.context = context;
        sharedPreferences = this.context.getSharedPreferences(NOME_ARQUIVO,MODE);
        editor = sharedPreferences.edit();
    }

    public void salvaConfig(String config){
        editor.putString(CONFIG,config);
        editor.commit();
    }

    public String buscaConfig(){
        if(sharedPreferences!=null) {
            return sharedPreferences.getString(CONFIG, null);
        }else{
            return null;
        }
    }

    public void salvar(String records){
        editor.putString(RECORDS,records);
        editor.commit();
    }

    public String buscar(){
        if(sharedPreferences!=null) {
            return sharedPreferences.getString(RECORDS, null);
        }else{
            return null;
        }
    }

    public String buscarRapido(){
        if(sharedPreferences!=null) {
            return sharedPreferences.getString(RECORDS2, null);
        }else{
            return null;
        }
    }

    public String buscarNiveis(){
        if(sharedPreferences!=null) {
            return sharedPreferences.getString(RECORDS3, null);
        }else{
            return null;
        }
    }

    public String buscarHard(){
        if(sharedPreferences!=null) {
            return sharedPreferences.getString(RECORDS4, null);
        }else{
            return null;
        }
    }

}
