package com.ifonelse.mathbarrier;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mtayl on 01/12/2017.
 */

public class RecordsAdapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<String> records;

    public RecordsAdapter (Context c, ArrayList<String> objects) {
        super(c, 0, objects);
        this.context = c;
        this.records = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;

        // Inicializa objeto para montagem do layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        // Monta view a partir do xml
        view = inflater.inflate(R.layout.item_records, parent, false);

        // Recupera elemento para exibição
        TextView txtNome = view.findViewById(R.id.txtNome);
        TextView txtPontos = view.findViewById(R.id.txtPontos);

        Typeface font = Typeface.createFromAsset(context.getAssets(),"vt323.ttf");
        txtNome.setTypeface(font);
        txtPontos.setTypeface(font);

        // Verifica se a lista está preenchida
        if( records != null ) {
            // Recupera usuario
            String record = records.get(position);

            String[] valor = record.split("-");
            txtNome.setText(valor[1]);
            txtPontos.setText(valor[0]);
        }else {
            txtNome.setText("NENHUM RECORD FOI ARMAZENADO AINDA!!");
        }
        return view;
    }


}