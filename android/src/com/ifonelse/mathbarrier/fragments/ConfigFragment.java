package com.ifonelse.mathbarrier.fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ifonelse.mathbarrier.MainActivity;
import com.ifonelse.mathbarrier.Preferencias;
import com.ifonelse.mathbarrier.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigFragment extends Fragment {


    private CheckBox efeitos,musica;
    private TextView creditos;
    private TextView comoJogar;
    private Typeface font;

    public MainActivity mainActivity;

    private Preferencias preferences;

    private WebView webView;
    public ConfigFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_config, container, false);

        webView = view.findViewById(R.id.how_play);

        preferences = new Preferencias(getActivity());

        efeitos = view.findViewById(R.id.chkEfeitos);
        musica = view.findViewById(R.id.chkMus);
        creditos = view.findViewById(R.id.txtCreditos);
        comoJogar = view.findViewById(R.id.txtComo);

        font = Typeface.createFromAsset(getActivity().getAssets(),"vt323.ttf");

        efeitos.setTypeface(font);
        musica.setTypeface(font);
        creditos.setTypeface(font);

        String[] configs = preferences.buscaConfig().split("_");
        efeitos.setChecked(configs[0].equals("+"));
        musica.setChecked(configs[1].equals("+"));

        efeitos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String config = "";
                if(b){
                    config+="+";
                }else{
                    config+="-";
                }

                config+="_";

                if(musica.isChecked()){
                    config+="+";
                }else{
                    config+="-";
                }
                preferences.salvaConfig(config);
            }
        });

        musica.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String config = "";
                if(efeitos.isChecked()){
                    config+="+";
                }else{
                    config+="-";
                }

                config+="_";

                if(b){
                    config+="+";
                }else{
                    config+="-";
                }
                preferences.salvaConfig(config);
            }
        });

        comoJogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.post(new Runnable() {
                    @Override
                    public void run() {

                        webView.loadUrl("file:///android_asset/comojogar.html");

                        webView.setBackgroundColor(Color.WHITE);
                        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

                    }
                });
            }
        });
        return view;
    }

}
