package com.ifonelse.mathbarrier.fragments;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.ifonelse.mathbarrier.MainActivity;
import com.ifonelse.mathbarrier.Preferencias;
import com.ifonelse.mathbarrier.R;


public class MainFragment extends Fragment implements View.OnClickListener{


    private ImageView btnClassico;
    private ImageView btnRank;
    private ImageView btnNiveis;
    private ImageView btnHardcore;
    private ImageView btnRapido;
    private ImageView btnConfig;

    AnimationSet growShrink;
    MediaPlayer toqueBotao;
    Preferencias preferences;
    private boolean tocaAudio=true;

    public MainActivity mainActivity;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_main, container, false);

        preferences = new Preferencias(getActivity());
        String config = preferences.buscaConfig();
        if (config == null) preferences.salvaConfig("+_+");

        btnClassico = view.findViewById(R.id.btnClassico);
        btnRank = view.findViewById(R.id.btnRank);
        btnNiveis = view.findViewById(R.id.btnNiveis);
        btnHardcore =  view.findViewById(R.id.btnHardcore);
        btnRapido = view.findViewById(R.id.btnRapido);
        btnConfig = view.findViewById(R.id.btnConfig);


        btnClassico.setOnClickListener(this);
        btnRank.setOnClickListener(this);
        btnNiveis.setOnClickListener(this);
        btnHardcore.setOnClickListener(this);
        btnRapido.setOnClickListener(this);
        btnConfig.setOnClickListener(this);


        RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(500);

        Animation animationScaleUp = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_up);
        Animation animationScapeDown = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_down);

        growShrink = new AnimationSet(true);
        growShrink.addAnimation(animationScaleUp);
        growShrink.addAnimation(animationScapeDown);
        growShrink.setInterpolator(new LinearInterpolator());

        String configs[] = preferences.buscaConfig().split("_");
        if(configs[0].equals("-")){
            tocaAudio = false;
        }

        toqueBotao = MediaPlayer.create(getActivity(), R.raw.toque);


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnClassico:
                btnClassico.startAnimation(growShrink);
                mainActivity.callClassico();
                break;
            case R.id.btnRapido:
                btnRapido.startAnimation(growShrink);
                mainActivity.callRapido();
                break;
            case R.id.btnNiveis:
                btnNiveis.startAnimation(growShrink);
                mainActivity.callNiveis();
                break;
            case R.id.btnHardcore:
                btnHardcore.startAnimation(growShrink);
                mainActivity.callHardcore();
                break;
            case R.id.btnConfig:
                btnConfig.startAnimation(growShrink);
                mainActivity.callConfig();
                break;
            case R.id.btnRank:
                btnRank.startAnimation(growShrink);
                mainActivity.callRank();
                break;
        }
        if(tocaAudio) {
            if (toqueBotao != null) {
                toqueBotao.start();
            }
        }
    }
}
