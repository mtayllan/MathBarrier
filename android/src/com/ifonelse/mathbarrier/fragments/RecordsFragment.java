package com.ifonelse.mathbarrier.fragments;


import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ifonelse.mathbarrier.MainActivity;
import com.ifonelse.mathbarrier.Preferencias;
import com.ifonelse.mathbarrier.R;
import com.ifonelse.mathbarrier.RecordsAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecordsFragment extends Fragment {

    private ListView lista_records;
    private ArrayAdapter adapter;
    private ArrayList<String> records;
    private TextView txtModo;
    private Gson gson;
    private Typeface font;


    private ImageView btnClassico;
    private ImageView btnNiveis;
    private ImageView btnHardcore;
    private ImageView btnRapido;


    private MediaPlayer audio;
    private boolean tocaAudio=true;

    Preferencias preferencias;

    public MainActivity mainActivity;

    public RecordsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_records, container, false);

        preferencias = new Preferencias(getActivity());
        String config[] = preferencias.buscaConfig().split("_");
        if(config[0].equals("-")){
            tocaAudio = false;
        }
        audio = MediaPlayer.create(getActivity(),R.raw.toque);

        font = Typeface.createFromAsset(getActivity().getAssets(),"vt323.ttf");

        txtModo = view.findViewById(R.id.txtModo);
        txtModo.setText("Clássico");
        txtModo.setTypeface(font);

        btnClassico  = view.findViewById(R.id.btnClassico);
        btnNiveis   = view.findViewById(R.id.btnNiveis);
        btnHardcore  = view.findViewById(R.id.btnHard);
        btnRapido    = view.findViewById(R.id.btnRapido);


        gson = new Gson();
        records = gson.fromJson(preferencias.buscar(), ArrayList.class);
        lista_records = view.findViewById(R.id.listaRe);
        if(records == null) {records = new ArrayList<>(); records.add("0-Alan");}
        adapter = new RecordsAdapter(getActivity(), records);
        lista_records.setAdapter(adapter);

        btnClassico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                records = gson.fromJson(preferencias.buscar(), ArrayList.class);
                if(records == null) {records = new ArrayList<>(); records.add("0-Alan");}
                adapter = new RecordsAdapter(getActivity(), records);
                lista_records.setAdapter(adapter);
                txtModo.setText("Clássico");
                if(tocaAudio)audio.start();
            }
        });

        btnRapido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                records = gson.fromJson(preferencias.buscarRapido(), ArrayList.class);
                if(records == null) {records = new ArrayList<>(); records.add("0-Alan");}
                adapter = new RecordsAdapter(getActivity(), records);
                lista_records.setAdapter(adapter);
                txtModo.setText("Rápido");
                if(tocaAudio)audio.start();
            }
        });

        btnNiveis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                records = gson.fromJson(preferencias.buscarNiveis(), ArrayList.class);
                if(records == null) {records = new ArrayList<>(); records.add("0-Alan");}
                adapter = new RecordsAdapter(getActivity(), records);
                lista_records.setAdapter(adapter);
                txtModo.setText("Níveis");
                if(tocaAudio)audio.start();
            }
        });

        btnHardcore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                records = gson.fromJson(preferencias.buscarHard(), ArrayList.class);
                if(records == null) {records = new ArrayList<>(); records.add("0-Alan");}
                adapter = new RecordsAdapter(getActivity(), records);
                lista_records.setAdapter(adapter);
                txtModo.setText("Hardcore");
                if(tocaAudio)audio.start();
            }
        });


        return view;
    }

}
