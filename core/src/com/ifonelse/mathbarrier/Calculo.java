package com.ifonelse.mathbarrier;

import com.badlogic.gdx.Gdx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.EventObject;

/**
 * Created by mtayl on 27/11/2017.
 */

public class Calculo {

    private String calculo;
    private boolean resposta;
    private Collection<GoListener> goListeners = new ArrayList<GoListener>();

    public Calculo(String calculo, boolean resposta){
        this.calculo = calculo;
        this.resposta = resposta;
    }

    public void setCalculo(String calculo, boolean resposta){
        this.calculo = calculo;
        this.resposta = resposta;
        disparaObjetoMudou();
    }

    public String getCalculo() {
        return calculo;
    }

    public void setCalculo(String calculo) {
        this.calculo = calculo;
    }

    public boolean isResposta() {
        return resposta;
    }

    public void setResposta(boolean resposta) {
        this.resposta = resposta;
    }

    public synchronized void objMudouListener(GoListener l) {
        if (!goListeners.contains(l)) {
            goListeners.add(l);
        }
    }
    private void disparaObjetoMudou() {
        Collection<GoListener> gl;
        synchronized (this) {
            gl = (Collection)(((ArrayList)goListeners).clone());
        }
        GoEvent evento = new GoEvent(this);
        for (GoListener g : gl) {
            g.objetoMudou(evento);
        }
    }
}

class GoEvent extends EventObject {
    public GoEvent(Calculo source) {
        super(source);
    }
}
interface GoListener extends EventListener {
    void objetoMudou(GoEvent evt);
}
