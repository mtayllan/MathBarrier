package com.ifonelse.mathbarrier;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import de.tomgrill.gdxdialogs.core.GDXDialogs;
import de.tomgrill.gdxdialogs.core.GDXDialogsSystem;
import de.tomgrill.gdxdialogs.core.dialogs.GDXButtonDialog;
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt;
import de.tomgrill.gdxdialogs.core.listener.ButtonClickListener;
import de.tomgrill.gdxdialogs.core.listener.TextPromptListener;

public class MathBarrier_Rapido extends ApplicationAdapter {

    Calculo calculo;
    Preferences preferences;

    /** Variaveis GDX */
    private SpriteBatch batch;
    private float deltaTime;

    /** Câmera + Dispositivo*/
    private OrthographicCamera camera;
    private Viewport viewport;
    private float larguraDispositivo;
    private float alturaDispositivo;

    /** Texturas */
    private Texture[] parado;
    private Texture[] correndo;
    private Texture[] pulo;
    private Texture[] deslize;
    private Texture fundo;
    private Texture obstaculo;

    /** Variáveis do Jogo */
    private int estado = 0;
    private int movimento = 0;
    private float variacaoParado = 0;
    private float variacaoCorrendo = 0;
    private boolean perdeu;
    private boolean podeResponder;
    private float velocidade = 1;

    /** Propriedades das Texturas*/
    private float larguraPersonagem;
    private float alturaPersonagem;
    private float larguraObstaculo;
    private float alturaObstaculo;
    private float personagemX;
    private float personagemY;
    private float altura;
    private float posicaoHorizontalFundo=0;
    float posicaoHorizontalObstaculo;

    /** Textos */
    private BitmapFont texto;
    private BitmapFont pontos;
    private GlyphLayout layout;
    private float layoutX=0;
    private float layoutY=0;

    /** Gerador + Pontuação */
    private int pontuacao = 0;
    private int contadorObstaculo = 3;
    private Random randomico;
    private int numeroAleatorio;
    private boolean trocaEquacao;
    private boolean resposta;
    private int numeroTotalEquacoes;
    private String equacao;
    private String[][] equacoes;

    /** Variáveis de Colisão */
    private Rectangle correPersonagem;
    private ShapeRenderer shape;

    /** Áudios */
    private Music jump;
    private Music dash;
    private Music background;
    private Music lose;

    private GDXDialogs dialogs;
    private Gson gson;

    @Override
    public void create () {  // Instanciando Valores Padrões

        //GDX
        batch = new SpriteBatch();

        //Câmera
        camera = new OrthographicCamera();
        float VIRTUAL_WIDTH = 1280;
        float VIRTUAL_HEIGHT = 768;
        camera.position.set(VIRTUAL_WIDTH /2, VIRTUAL_HEIGHT /2, 0);
        viewport = new StretchViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, camera);

        //Dispositivo
        larguraDispositivo = VIRTUAL_WIDTH;
        alturaDispositivo  = VIRTUAL_HEIGHT;

        //Textos

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("vt323.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        parameter.color = Color.WHITE;

        texto = generator.generateFont(parameter);
        pontos = generator.generateFont(parameter);
        generator.dispose(); // don't forget to dispose to avoid memory leaks!

        randomico = new Random();

        equacoes =  Equacoes.getAll();
        numeroTotalEquacoes = equacoes.length;

        //Texturas
        parado = new Texture[2];
        parado[0] = new Texture("parado1.png");
        parado[1] = new Texture("parado2.png");

        correndo = new Texture[5];
        correndo[0] = new Texture("correndo1.png");
        correndo[1] = new Texture("correndo2.png");
        correndo[2] = new Texture("correndo3.png");
        correndo[3] = new Texture("correndo4.png");
        correndo[4] = new Texture("correndo5.png");

        pulo = new Texture[8];
        pulo[0] = new Texture("pulando1.png");
        pulo[1] = new Texture("pulando2.png");
        pulo[2] = new Texture("pulando3.png");
        pulo[3] = new Texture("pulando4.png");
        pulo[4] = new Texture("pulando4.png");
        pulo[5] = new Texture("pulando3.png");
        pulo[6] = new Texture("pulando2.png");
        pulo[7] = new Texture("pulando1.png");

        deslize = new Texture[5];
        deslize[0] = new Texture("escorregando1.png");
        deslize[1] = new Texture("escorregando2.png");
        deslize[2] = new Texture("escorregando3.png");
        deslize[3] = new Texture("escorregando4.png");
        deslize[4] = new Texture("escorregando5.png");

        fundo = new Texture("fundo.png");
        obstaculo = new Texture("obstaculo.png");

        //Personagem + Obstaculo
        larguraPersonagem = correndo[0].getWidth()/3;
        alturaPersonagem = correndo[0].getHeight()/3;
        larguraObstaculo = obstaculo.getWidth()*3/10;
        alturaObstaculo =  obstaculo.getHeight()*3/10;

        personagemX = fundo.getWidth()/30;
        personagemY = fundo.getHeight()/20;
        altura = personagemY;
        posicaoHorizontalObstaculo = (larguraDispositivo * contadorObstaculo);

        //Preferencias
        gson = new Gson();;
        preferences = Gdx.app.getPreferences("MATHBARRIER-PREFERENCIAS");
        dialogs = GDXDialogsSystem.install();

        String audios[] = preferences.getString("configuracoes").split("_");

        //Musicas
        if(audios[0].equals("+")) {
            jump = Gdx.audio.newMusic(Gdx.files.internal("song/jump.wav"));
            dash = Gdx.audio.newMusic(Gdx.files.internal("song/dash.wav"));
            lose = Gdx.audio.newMusic(Gdx.files.internal("song/lose.mp3"));
        }
        if(audios[1].equals("+")) {
            background = Gdx.audio.newMusic(Gdx.files.internal("song/back.mp3"));
            background.setLooping(true);
            background.play();
        }

        //Movimentos Slide
        Gdx.input.setInputProcessor(new SimpleDirectionGestureDetector(new SimpleDirectionGestureDetector.DirectionListener() {

            @Override
            public void onUp() {
                if(podeResponder) {
                    movimento = 1;
                    trocaEquacao = true;
                    variacaoCorrendo = 0;
                    podeResponder = false;
                    if(jump!=null)jump.play();
                }
            }

            @Override
            public void onRight() {

            }

            @Override
            public void onLeft() {

            }

            @Override
            public void onDown() {
                if(podeResponder) {
                    trocaEquacao = true;
                    movimento = 2;
                    variacaoCorrendo = 0;
                    podeResponder = false;
                    if(dash!=null)dash.play();
                }
            }
        }));
        alan = new Texture[2];
        alan[0] = new Texture("alan.png");
        alan[1] = new Texture("alan2.png");

        felipe = new Texture[2];
        felipe[0] = new Texture("felipe.png");
        felipe[1] = new Texture("felipe2.png");

        professores = new Texture[][]{
                alan,
                felipe
        };

        alturaProfs = alan[0].getHeight()/2;
        larguraProfs = alan[0].getWidth()/2;

        posicaoHorizontalProfs = larguraDispositivo + alan[0].getWidth();

    }

    @Override
    public void render () {

        //Captura o deltatime para reproduzir animações
        deltaTime = Gdx.graphics.getDeltaTime() * velocidade;

        // Atualiza camera e limpa frames anteriores
        camera.update();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        //Configurar dados de projeção da câmera
        batch.setProjectionMatrix(camera.combined);

        //Desenha animação
        batch.begin();


        if (estado == 0) {
            if (Gdx.input.justTouched()) {
                estado = 1;
                trocaEquacao = true;
            }
            parado();

            layout = new GlyphLayout(texto, "Clique para iniciar!");
            layoutX = (larguraDispositivo - layout.width) / 2;
            layoutY = (alturaDispositivo + layout.height) / 2;
            texto.draw(batch, layout, layoutX, layoutY);

        } else if (estado == 1) {
            fundoMovendo();
            personagens();
            if (movimento == 0) {
                correndo();
            } else if (movimento == 1) {
                pular();
            } else if (movimento == 2) {
                deslizar();
            }
            movimentoObstaculo();
            colisao();
        } else if (estado == 2) {
            parado();
            gameover();
            if (Gdx.input.justTouched()) {
                recarregaVariaveis();
            }
        }

        batch.end();

        /*
        shape.setProjectionMatrix(camera.combined);

        shape.begin(ShapeRenderer.ShapeType.Line);

        shape.rect(hpX, hpY, larguraPersonagem / 2, hpAltura);
        shape.rect(posicaoHorizontalObstaculo + larguraObstaculo / 4, alturaObstaculo * 9 / 10, larguraObstaculo / 2, alturaObstaculo / 11);
        shape.end();

        */

    }


    private void fundoMovendo(){
        posicaoHorizontalFundo-=larguraDispositivo * deltaTime/2;
        if(-posicaoHorizontalFundo >= larguraDispositivo){
            posicaoHorizontalFundo = 0;
            contadorObstaculo--;

            if(contaProfs!=0) {
                contaProfs--;
                desenhaProfs = false;
            }

        }

        batch.draw(fundo,posicaoHorizontalFundo,0,larguraDispositivo,alturaDispositivo);
        batch.draw(fundo,posicaoHorizontalFundo+larguraDispositivo,0,larguraDispositivo,alturaDispositivo);
        pontos.draw(batch,"Pontos:"+String.valueOf(pontuacao),(larguraDispositivo/5)*4,(alturaDispositivo / 10));
    }

    private void movimentoObstaculo(){
        posicaoHorizontalObstaculo -= larguraDispositivo*deltaTime/2;
        if(posicaoHorizontalObstaculo < larguraDispositivo-larguraObstaculo){
            podeResponder = true;
        }
        if(posicaoHorizontalObstaculo < personagemX){
            podeResponder = false;
        }
        if(posicaoHorizontalObstaculo<-larguraObstaculo+10){
            contadorObstaculo = 3;
            posicaoHorizontalObstaculo = (larguraDispositivo * contadorObstaculo);
        }
        batch.draw(obstaculo,posicaoHorizontalObstaculo,personagemY,larguraObstaculo,alturaObstaculo);
    }

    private void parado(){
        variacaoParado += deltaTime * 3;
        if(variacaoParado>2){
            variacaoParado = 0;
        }

        Texture tex = parado[(int) variacaoParado];

        batch.draw(fundo,0,0,larguraDispositivo,alturaDispositivo);
        batch.draw(tex,personagemX, personagemY,larguraPersonagem,alturaPersonagem);
    }

    private void correndo(){
        variacaoCorrendo += deltaTime * 8;
        if(variacaoCorrendo>5){
            variacaoCorrendo = 0;
        }
        if(trocaEquacao) {

            numeroAleatorio = randomico.nextInt(numeroTotalEquacoes);
            equacao = equacoes[numeroAleatorio][0];
            resposta = Boolean.parseBoolean(equacoes[numeroAleatorio][1]);

            layout = new GlyphLayout(texto, equacao);
            layoutX = (larguraDispositivo - layout.width) / 2;
            layoutY = (alturaDispositivo / 10) * 9;
            trocaEquacao = false;
            calculo.setCalculo(equacao,true);
            velocidade+=0.05;
        }

        Texture tex = correndo[(int)variacaoCorrendo];

        batch.draw(tex,personagemX, personagemY,larguraPersonagem,alturaPersonagem);
    }

    private void deslizar(){

        if(!resposta) {
            calculo.setCalculo("");
            variacaoCorrendo += deltaTime * 6;
            if (variacaoCorrendo > 5) {
                movimento = 0;
                variacaoCorrendo = 0;
                pontuacao++;
            }

            Texture tex = deslize[(int) variacaoCorrendo];
            batch.draw(tex, personagemX, personagemY,larguraPersonagem,alturaPersonagem);
        } else{
            estado = 2;
        }
    }

    private void pular(){
        if(resposta) {
            variacaoCorrendo += deltaTime * 6;
            calculo.setCalculo("");
            if((int)variacaoCorrendo <= 4){
                altura += alturaDispositivo/80;
            }else if(variacaoCorrendo>4 && variacaoCorrendo<=8){
                altura -= alturaDispositivo/80;
            }

            if(altura>(alturaDispositivo/2)){
                altura = alturaDispositivo/2;
            }else if(altura<personagemY){
                altura = personagemY;
            }


            if (variacaoCorrendo > 8) {
                movimento = 0;
                variacaoCorrendo = 0;
                pontuacao++;
                altura = personagemY;
            }

            Texture tex = pulo[(int) variacaoCorrendo];
            batch.draw(tex, personagemX, altura,larguraPersonagem,alturaPersonagem);
        }else{
            estado = 2;
        }
    }

    private void colisao(){
        float hpX = personagemX+larguraPersonagem/4;
        float hpY = altura+alturaPersonagem/10;
        float hpAltura = alturaPersonagem/(3/2);

        if(movimento == 0){
            hpAltura = alturaPersonagem;
        }else if(movimento == 1){
            hpY += hpAltura/10;
            hpX = hpX*3/5;
        }else if(movimento == 2){
            hpAltura = hpAltura/2;
        }

        Rectangle hitPersonagem = new Rectangle(hpX,hpY,larguraPersonagem/2,hpAltura);
        Rectangle hitObstaculo = new Rectangle(posicaoHorizontalObstaculo+larguraObstaculo/4,alturaObstaculo*9/10,larguraObstaculo/2,alturaObstaculo/11);

        if(Intersector.overlaps(hitPersonagem,hitObstaculo)){
            estado = 2;
        }
    }

    private void gameover(){
        if(!perdeu) {
            String ponto = "";
            if(pontuacao<10){
                ponto = "00"+String.valueOf(pontuacao);
            }else if(pontuacao>=10 && pontuacao<100){
                ponto = "0"+String.valueOf(pontuacao);
            }else{
                ponto = String.valueOf(pontuacao);
            }
            final String pontos = ponto;
            layout = new GlyphLayout(texto, "Você perdeu! Fez " + pontos + " pontos!");
            layoutX = (larguraDispositivo - layout.width) / 2;
            layoutY = (alturaDispositivo + layout.height) / 2;
            perdeu = true;
            if(lose!=null)lose.play();

            final GDXTextPrompt textPrompt = dialogs.newDialog(GDXTextPrompt.class);
            textPrompt.setTitle("Gravar Pontuação");
            textPrompt.setMessage("Digite seu nome:");

            textPrompt.setCancelButtonLabel("Cancelar");
            textPrompt.setConfirmButtonLabel("Salvar");

            textPrompt.setTextPromptListener(new TextPromptListener() {

                @Override
                public void confirm(String text) {
                    if (text.equals("") || text.startsWith(" ") || text.contains("-")) {
                        final GDXButtonDialog bDialog = dialogs.newDialog(GDXButtonDialog.class);
                        bDialog.setTitle("Erro");
                        bDialog.setMessage("Nome inválido!");

                        bDialog.setClickListener(new ButtonClickListener() {

                            @Override
                            public void click(int button) {
                                bDialog.dismiss();
                                textPrompt.build().show();
                            }
                        });

                        bDialog.addButton("Ok");
                        bDialog.build().show();
                    } else {
                        ArrayList<String> records = gson.fromJson(preferences.getString("records_rapido"), ArrayList.class);
                        if (records == null)records = new ArrayList<String>();
                        if(records.size()<10){
                            records.add(pontos + "-" + text);
                        }else{
                            for(int i=9; i>=0; i--){
                                String valor = records.get(i).split("-")[0];
                                if (Integer.parseInt(pontos) > Integer.parseInt(valor)){
                                    records.set(i,pontos+ "-" + text);
                                    break;
                                }
                            }
                        }

                        Collections.sort(records);
                        Collections.reverse(records);

                        String data = gson.toJson(records);
                        preferences.putString("records_rapido", data);
                        preferences.flush();
                    }
                    textPrompt.dismiss();
                }

                @Override
                public void cancel() {
                    textPrompt.dismiss();
                }
            });

            textPrompt.build().show();

        }



        texto.draw(batch, layout, layoutX, layoutY);
        pontuacao=0;
    }

    private void recarregaVariaveis(){
        estado = 0;
        trocaEquacao = true;
        perdeu = false;
        movimento = 0;
        contadorObstaculo=3;
        posicaoHorizontalObstaculo = larguraDispositivo*contadorObstaculo;
        podeResponder = false;
        altura = personagemY;
        velocidade = 1;
        variacaoProfs = 0;
        posicaoHorizontalProfs = larguraDispositivo + larguraProfs;
        alOufl= 0;
        contaProfs = 10;
        desenhaProfs = false;
    }

    private Texture[] alan;
    private Texture[] felipe;
    private float posicaoHorizontalProfs;
    private float alturaProfs;
    private float larguraProfs;
    private float variacaoProfs = 0;
    private int contaProfs = 10;
    private boolean desenhaProfs = false;
    private Texture[][] professores;
    private int alOufl = 0;

    private void personagens() {
        if (contaProfs == 0) {
            desenhaProfs = true;
        }

        if (desenhaProfs) {
            variacaoProfs += deltaTime * 5;
            posicaoHorizontalProfs -= larguraDispositivo * deltaTime / 2;
            if (posicaoHorizontalProfs < -larguraProfs) {
                posicaoHorizontalProfs = larguraDispositivo + larguraProfs;
                desenhaProfs = false;
                contaProfs = 10;
                alOufl = alOufl == 0 ? 1 : 0;
            }

            if (variacaoProfs > 2) {
                variacaoProfs = 0;
            }

            Texture tex = professores[alOufl][(int)variacaoProfs];
            batch.draw(tex, posicaoHorizontalProfs - larguraDispositivo / 20, personagemY + tex.getHeight() / 8, larguraProfs, alturaProfs);
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

}
